import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }

}
