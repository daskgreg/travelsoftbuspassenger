import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { LanguagePopoverPage } from '../language-popover/language-popover.page';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  constructor(private router: Router, private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  pageNavigationToRegister(){
    this.router.navigate(['/register'])
  }
  pageNavigationToLogin(){
    this.router.navigate(['home'])
  }

  async openLanguagePopover(ev){
    const popover = await this.popoverCtrl.create({
      component:LanguagePopoverPage,
      event:ev
    });
    await popover.present();
  }

}
