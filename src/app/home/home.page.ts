import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, PickerController } from '@ionic/angular';
import { PickerOptions } from "@ionic/core";
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { IonSearchbar } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  myDate = new Date().toISOString();

  @ViewChild('search', { static: false }) search:IonSearchbar;

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  aDate:any;
  showSafetyMeasures = false;
  selectPeople = false;
  footer = true;
  peopleShow = false;
  peoplebutton = true;
  isKeyboard = true;
  isSearchedForDeparture = false;
  isSearchedForDeparture2 = true;
  isSearchedForDeparture3 = false;
  isSearchedForArrival = false;
  isSearchedForArrival2 = true;
  isSearchedForArrival3 = false;
  selectedDeparture:any
  myDeparture:any;
  myArrival:any;
  adults:any;
  children:any;
  enfants:any;
  myArrival_PUP_CODE:any;
  myDeparture_PUP_CODE:any;
  dateToShow:any;
  dateToSowGen:any;
  public data: Array<Object> = [];
  private searchedItem:any;

  constructor(private http: HTTP,public keyboard: Keyboard,private router: Router,private loadingCtrl:LoadingController,private alertCtrl:AlertController,private pickerController:PickerController) {
   
    this.getLocations();
    
  }

  dateChanged(date){
    console.log(date.detail.value);
    this.myDate = date.detail.value;
    this.myDate = moment(this.myDate).format('YYYY-DD-MM')
    console.log(this.myDate);
    this.dateToShow = moment(this.myDate).format('MMMM Do YYYY, h:mm:ss a')
    localStorage.setItem('dateToShowForRoute',this.dateToShow);
    this.dateToSowGen = localStorage.getItem('dateToShowForRoute');
    
  }
  getDepartures(item:any){
    this.myDeparture = item.PUP_DESC;
    this.myDeparture_PUP_CODE = item.PUP_CODE;
    this.isSearchedForDeparture = false;
    this.isSearchedForDeparture2 = false;
    this.isSearchedForDeparture3 = true;
  }
  getArrivals(item:any){
    this.myArrival = item.PUP_DESC;
    this.myArrival_PUP_CODE = item.PUP_CODE;
    this.isSearchedForArrival = false;
    this.isSearchedForArrival2 = false;
    this.isSearchedForArrival3 = true;
  }
  async getLocations(){
    let loading = await this.loadingCtrl.create();
    await loading.present();

    let nativeCall = this.http.get('http://cf11.travelsoft.gr/itourapi/trp_destination_departure_list.cfm?userid=dmta', {}, {
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    from(nativeCall).pipe(
      finalize( ()=>loading.dismiss())
    )

    .subscribe(data => {
      let parsed = JSON.parse(data.data).DEPDEST
      this.data = parsed;
      this.searchedItem = this.data

    }, err => {
      console.log('JS Call error:',err);
    });
   this.searchedItem = this.data;
  }
  async ionViewDidEnter(){
    
   
    setTimeout(() =>{
      this.search.setFocus();
    })
  }
  ionSearchDep(event){
    const val = event.target.value;
    this.searchedItem = this.data;
    this.isSearchedForDeparture = true;
    if(val && val.trim() != ''){
      this.searchedItem = this.searchedItem.filter((item:any) => {
        return (item.PUP_DESC.toLowerCase().indexOf(val.toLowerCase()) > -1 );

      })
    
    }
  }
  ionSearchArr(event){
    const val = event.target.value;
    this.searchedItem = this.data;
    this.isSearchedForArrival = true;
    if(val && val.trim() != ''){
      this.searchedItem = this.searchedItem.filter((item:any) => {
        return (item.PUP_DESC.toLowerCase().indexOf(val.toLowerCase()) > -1 );

      })
    
    }
  }
  async presentAlert() {
    let alert = this.alertCtrl.create({
      header: 'Μετρά Προστασίας',
      subHeader: 'Καθαρίζετε συχνά τα χέρια σας Βήχας ή φτάρνισμα στον λυγισμένο αγκώνα σας - όχι στα χέρια σας! Αποφύγετε να αγγίξετε τα μάτια, τη μύτη και το στόμα σας Περιορίστε τις κοινωνικές συγκεντρώσεις και το χρόνο που περνάτε σε πολυσύχναστα μέρη Αποφύγετε τη στενή επαφή με κάποιον που είναι άρρωστος Καθαρίστε και απολυμάνετε αντικείμενα και επιφάνειες που αγγίζονται συχνά',
      buttons: ['OK']
    });
    (await alert).present();
  }
  showOptionsOfPeople(){
    if(this.peopleShow == false){
      this.peopleShow = true;
      this.peoplebutton = false;
    }
  }
  sPeople(){
    this.selectPeople = true;
    this.footer = false;
  }
  navigateToRoute(){
    localStorage.setItem('departure', this.myDeparture);
    localStorage.setItem('departurePupCode', this.myDeparture_PUP_CODE);
    localStorage.setItem('arrival', this.myArrival);
    localStorage.setItem('arrivalPupCode', this.myArrival_PUP_CODE);
    localStorage.setItem('date', this.myDate);
    localStorage.setItem('adults', this.adults);
    localStorage.setItem('children', this.children);
    localStorage.setItem('enfants', this.enfants);
    console.log(this.adults);

    this.router.navigate(['routes']);
  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }

 

}
