
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
declare var google: any;
@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage {

  bookingID = '0001';
  qrData :any;
  scannedCode = null;
  elementType: 'url' | 'canvas' | 'img' = 'canvas';
  backendAPIDATA:any;
  trips: any[];
  // tripColFirst: any[];
  // tripColSecond: any[];
  // tripColThird: any[];
  // tripColForth: any[];
  // tripColFifth: any[];

  flooerColumns: any[];
  tempBookedSeats: any[];
  dropOffStations: any[];
  totalSeatsPrice: number = 0;
  PICKUP: string = "";
  dropOff: string;
  lat: number = 0;
  long: number = 0;

  dataFromSelectStart: any;
  dataFromSelectStartParsed: any;
  parseThisData: any;

  // This is the variables for the form
  showSeatPage = false;
  showSeatForm = true;
  procceedNext = false;

  getAdultsNumber = 0;
  getChildrenNumber = 0;
  getInfantsNumber = 0;

  peopleFormForSeats = {
    getAdultsNumber: '0',
    getChildrenNumber: '0',
    getInfantsNumber: '0'
  }
  totalPeople: any;
  countSeats = 0;
  address: any;
  imgTrue = false;
  imgFalse = true;


 FFpayment:any;
  constructor(private payPal: PayPal,private platform: Platform, private nativeHttp: HTTP, private http: HttpClient, public router: Router, private activatedRoute: ActivatedRoute, public navCtrl: NavController, public loadingCtrl: LoadingController, public actionSheet: ActionSheetController, public alertCtrl: AlertController, private barcodeScanner: BarcodeScanner,
    private base64ToGallery: Base64ToGallery,
    private toastCtrl:ToastController,
    private camera: Camera) {
    let _this = this;
    setTimeout(() => {
      // Render the PayPal button into #paypal-button-container
      <any>window['paypal'].Buttons({

        // Set up the transaction
        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [{
              amount: {
                value: _this.paymentAmount
              }
            }]
          });
        },
        //
        // Finalize the transaction
        onApprove: function (data, actions) {
          return actions.order.capture()
            .then(function (details) {
              console.log(data);
              console.log("details",details.status);
              _this.FFpayment = details.status;
              console.log(_this.FFpayment)
              if(details.status == 'COMPLETED'){
                _this.router.navigate(['main']);
              }
              if(_this.FFpayment == "COMPLETED"){
                _this.router.navigate(['main']);
              }
              
            })
            .catch(err => {
              console.log(err);
            })
        }
      }).render('#paypal-button-container');
    }, 500)

  }

  paymentAmount: string = '3.33';
  currency: string = 'INR';
  currencyIcon: string = '₹';

  todo = {

  }
  logForm() {
    console.log(this.todo)

    this.todo = JSON.stringify(this.todo);
    console.log(this.todo);
    this.qrData = this.todo;
    this.backendAPIDATA = this.todo;
  }


  scanCode() 
  {
    this.barcodeScanner.scan().then
    (
      barcodeData => 
      {
        this.scannedCode = barcodeData.text;
      }
    );
  }
  
  downloadQR(){
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();
    console.log('data: ',imageData);

    // Splitting the string of the image at the fist ,  
    let data = imageData.split(',')[1];

    // let data is the string that the backend-api will receive with 
    //  qrData :any;
    //  qrData is an array which is generating a barcode with specific values eg firstName: greg,lastName dask
    // in that way we make sure that every barcode is going to be unique 

    console.log("QR data:",data);
    
    // For Cordova Platforms Like Android and ios 
    this.base64ToGallery.base64ToGallery
    (data,
       { prefix: '_qrCodeImg', mediaScanner: true }
    ).then
    ( 
      async res => 
      {
        let toast = await this.toastCtrl.create({
          header:'QR code saved'
        });
        toast.present();
      },err => console.log('err:', err));
    }
    
  currentImage = null;
  captureImage(){
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI
    }

    this.camera.getPicture(options).then( imageData => {
      this.currentImage = imageData;
    },err => {
      console.log('Image Error: ', err);
    });
  }

  function(){
    if(this.FFpayment == true){
      this.router.navigate(['main']);
    }
  }

  gotomain(){
    this.router.navigate(['main']);
  }
}



  
  
