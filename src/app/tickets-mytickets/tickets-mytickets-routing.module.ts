import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsMyticketsPage } from './tickets-mytickets.page';

const routes: Routes = [
  {
    path: '',
    component: TicketsMyticketsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketsMyticketsPageRoutingModule {}
