import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tickets-mytickets',
  templateUrl: './tickets-mytickets.page.html',
  styleUrls: ['./tickets-mytickets.page.scss'],
})
export class TicketsMyticketsPage implements OnInit {

 
 
 
  constructor(private http:HttpClient,private router: Router) { }

  ngOnInit() {
  }
  bookingT:any;
  informationT:any;
  ionViewWillEnter(){
    this.http.get("assets/data/datatickets.json").subscribe(data =>{
      console.log(data);
      this.bookingT = data;
      this.informationT = this.bookingT.bookingTickets.information;
      console.log(this.bookingT.bookingTickets.information);
    })
  }
 
  getDataTickets(){
  
  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
}
