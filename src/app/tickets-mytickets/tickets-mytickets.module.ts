import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TicketsMyticketsPageRoutingModule } from './tickets-mytickets-routing.module';

import { TicketsMyticketsPage } from './tickets-mytickets.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TicketsMyticketsPageRoutingModule,
    TranslateModule
  ],
  declarations: [TicketsMyticketsPage]
})
export class TicketsMyticketsPageModule {}
