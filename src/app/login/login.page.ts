import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, Platform } from '@ionic/angular';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  response:any;
  phoneNumber:any;
  password:any;
  data = [];
  dataStr:any;
  dataRes:any;

  firstName:any;
  lastName:any;
  email:any;
  loginInformationdata:any;
  key:any;
  constructor(private router:Router, private http:HTTP, private loadingCtrl:LoadingController, private platform:Platform) { }

  ngOnInit() {
  }


  async login(){

    let loading = await this.loadingCtrl.create();

    await loading.present();

    let nativeCall = this.http.get('http://cf11.travelsoft.gr/itourapi/passengerapi/login.cfm?' + 'mobile=' + '1234567890' + '&password=' + 'Chr1stos' + '&userid=dmta', {}, {
    
    'Content-Type': 'application/x-www-form-urlencoded'

    });

    from(nativeCall).pipe(

      finalize( ()=> loading.dismiss())

    )

    .subscribe(data => {
     

      let myData = JSON.parse(data.data);

      if(myData.RESPONSE == 'success'){

        this.loginInformationdata = JSON.stringify(myData);

        localStorage.setItem('loginInformation',this.loginInformationdata);

        this.key = localStorage.getItem('loginInformation');

        this.router.navigate(['home']);
      }

    

    }, err => {

      console.log('JS Call error:',err);

    });
  }

  navigateToRegisterPage(){
    this.router.navigate(['register']);
  }

}
