import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.page.html',
  styleUrls: ['./services.page.scss'],
})
export class ServicesPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  navigateToLiveChat(){
    this.router.navigate(['livechat']);
  }
  serviceEmail(){

  }
  serviceCall(){

  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
}
