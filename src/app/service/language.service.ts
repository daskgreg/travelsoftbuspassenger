import { Injectable } from '@angular/core';
import {Platform} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})

export class LanguageService {

  selected = '';

  constructor(private translate:TranslateService,private storage: Storage,private platform:Platform) {}

  setInitialAppLanguage(){
    let language = this.translate.getBrowserLang();
    this.translate.setDefaultLang('gr');

    this.storage.get(LNG_KEY).then(val => {
      if(val){
        this.setLanguage(val);
        this.selected = val;
      }
    });
  }
  getLanguages(){
    return [
      { text: 'Greek', value: 'gr', img:'assets/icons/drawable-xxxhdpi/greece.png'},
      { text: 'English', value: 'en', img:'assets/icons/drawable-xxxhdpi/united-kingdom.png'},
    ];
  }
  setLanguage(lng){
    this.translate.use(lng);
    this.selected = lng;
    this.storage.set(LNG_KEY,lng);
  }
}
