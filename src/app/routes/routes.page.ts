import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, Platform } from '@ionic/angular';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.page.html',
  styleUrls: ['./routes.page.scss'],
})
export class RoutesPage{

  myDeparture:any;
  myArrival:any;
  myDeparturePupCode:any;
  myArrivalPupCode:any;
  myDate:any;
  myAdults:any;
  myChildren:any;
  myEnfants:any;

  data:any;
  dataFrom:any;
  dataTo:any;
  dataDate:any;
  dataSeats:any;
  dataArr:any;
  dataDepar:any;
  totalTimeOfTrip:any;
  trpDetails:any;
  totalPrice:any;
  dateToSowGen:any;
  gotItem:any;

  constructor(private router:Router, private http:HTTP, private loadingCtrl:LoadingController, private platform:Platform) { 

    this.myDeparture = localStorage.getItem('departure');
    this.myArrival = localStorage.getItem('arrival');
    this.myDeparturePupCode = localStorage.getItem('arrivalPupCode');
    this.myArrivalPupCode = localStorage.getItem('departurePupCode');
    this.myDate = localStorage.getItem('date');
    console.log(this.myDate);
    this.myAdults = localStorage.getItem('adults');
    this.myChildren = localStorage.getItem('children');
    this.myEnfants = localStorage.getItem('enfants');
    

    this.dateToSowGen = localStorage.getItem('dateToShowForRoute');
    console.log("fuck fuck",this.dateToSowGen);

    this.loadRoutes();
  }

  async loadRoutes(){

    let loading = await this.loadingCtrl.create();

    await loading.present();

    if(this.myAdults == 'undefined'){
      this.myAdults = '0';
    }
     if(this.myChildren == 'undefined'){
      this.myChildren = '0';
    }
     if(this.myEnfants == 'undefined'){
      this.myEnfants = '0'
    }
      
    
    

     let nativeCall = this.http.get('http://cf11.travelsoft.gr/itourapi/trp_avl.cfm?'

      + 'fpup_code=' + this.myDeparturePupCode

      + '&tpup_code=' +  this.myArrivalPupCode

      + '&fromd=' + this.myDate

      + '&tod=' +  this.myDate

      + '&adults=' +  this.myAdults

      + '&children=' +  this.myChildren

      + '&infants=' + this.myEnfants

      + '&userid=dmta', {}, {

      'Content-Type': 'application/json'

      });

      from(nativeCall).pipe(

        finalize( ()=> loading.dismiss())

      )
      .subscribe( data => {

        let parsed = JSON.parse(data.data).TRPS;

        this.data = parsed;

        this.dataFrom = this.data[0].FPUP_DESC;

        this.dataTo = this.data[0].TPUP_DESC;

        this.dataDate = this.myDate;

        this.dataArr = this.data[0].TRPDETAIS[0].DROPPING_TIME;

        this.dataDepar = this.data[0].TRPDETAIS[0].BOARDING_TIME;

        this.totalPrice = this.data[0].TRPDETAIS[0].TOTAL_PRICE_WITH_FEE;

        console.log(this.totalPrice);

        localStorage.setItem('FPUP_DESC',this.dataFrom);

        localStorage.setItem('TPUP_DESC',this.dataTo);

        localStorage.setItem('myDate',this.dataDate);

        localStorage.setItem('DROPPING_TIME',this.dataArr);

        localStorage.setItem('BOARDING_TIME',this.dataDepar);

        localStorage.setItem('TOTAL_PRICE_WITH_FEE',this.totalPrice);

        this.totalTimeOfTrip = parseFloat(this.dataArr)  - parseFloat(this.dataDepar);

        localStorage.setItem('calucationOfTime',this.totalTimeOfTrip);
      })
  }

  selectRoute(item:any){

    let constItem = JSON.stringify(item);

    localStorage.setItem('routeItem',constItem);

    this.gotItem = localStorage.getItem('routeItem');
   
    this.router.navigate(['checkout']);

  }
  navigateToLogin(){
    this.router.navigate(['checkout']);
  }
  goback(){
    this.router.navigate(['home']);
  }
}
