import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import 'reflect-metadata';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  constructor(private router: Router,) { }

  loginInformationdata:any;
  LID:any;
  LIDA:any;
  key:any;

  firstName:any;
  lastName:any;
  email:any;
  mobile:any;
  ngEmail:any;
  ngFirstName:any;
  ngLastName:any;
  ngMobile:any;

  ionViewDidEnter(){

    this.key = localStorage.getItem('loginInformation');

    this.LIDA = JSON.parse(this.key);

    this.firstName = this.LIDA.FIRSTNAME;

    this.lastName = this.LIDA.LASTNAME;

    this.email = this.LIDA.EMAIL;

    this.mobile = this.LIDA.MOBILE;

}
  ngOnInit() {
  }

  getDataFromForm(){
      if(this.ngEmail != undefined){

        this.ngEmail = this.ngEmail;

      }else {

        this.ngEmail = this.email;

      }

      if(this.ngFirstName != undefined){

        this.ngFirstName = this.ngFirstName;

      }else {

        this.ngFirstName = this.firstName;

      }
      if(this.ngLastName != undefined){

        this.ngLastName = this.ngLastName;

      }else {

        this.ngLastName = this.lastName;

      }
      if(this.ngMobile != undefined){

        this.ngMobile = this.ngMobile;

      }else {

        this.ngMobile = this.mobile;

      }

      localStorage.setItem('finalEmail',this.ngEmail);
      
      localStorage.setItem('FinalFirstName',this.ngFirstName);

      localStorage.setItem('FinalLastName',this.ngLastName);

      localStorage.setItem('FinalMobile',this.ngMobile);

      if( this.ngEmail == undefined || this.ngFirstName == undefined || this.ngLastName == undefined || this.ngMobile == undefined){
        
        alert('Συμπλήρωστε όλα τα πεδία');

      }else {

        this.router.navigate(['checkout-extras-payment']);

      }

  }
  goback(){

    this.router.navigate(['routes']);

  }
}
