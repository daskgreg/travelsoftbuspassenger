import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.page.html',
  styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
  navigateToMyTickets(){
    this.router.navigate(['tickets-mytickets']);
  }
  navigateToSearchTicket(){
    this.router.navigate(['tickets-search']);
  }

}
