import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { LanguagePopoverPage } from '../language-popover/language-popover.page';
@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {

  constructor(private router: Router, private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  async openLanguagePopover(ev){
    const popover = await this.popoverCtrl.create({
      component:LanguagePopoverPage,
      event:ev
    });
    await popover.present();
  }
}
