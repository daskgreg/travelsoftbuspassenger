import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TicketsSearchPageRoutingModule } from './tickets-search-routing.module';

import { TicketsSearchPage } from './tickets-search.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TicketsSearchPageRoutingModule,
    TranslateModule
  ],
  declarations: [TicketsSearchPage]
})
export class TicketsSearchPageModule {}
