import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsSearchPage } from './tickets-search.page';

const routes: Routes = [
  {
    path: '',
    component: TicketsSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketsSearchPageRoutingModule {}
