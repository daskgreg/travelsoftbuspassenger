import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { isWithinInterval } from 'date-fns/esm';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tickets-search',
  templateUrl: './tickets-search.page.html',
  styleUrls: ['./tickets-search.page.scss'],
})
export class TicketsSearchPage implements OnInit {

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  bookingT:any;
  informationT:any;
  filtered:any;
  startSearchDate:any;
  endSearchDate:any;
  searchmyticket = false;
  dateISMISSING = false;
  ionViewWillEnter(){
    this.http.get("assets/data/datatickets.json").subscribe(data =>{
      console.log(data);
      this.bookingT = data;
      this.informationT = this.bookingT.bookingTickets.information;
      console.log(this.bookingT.bookingTickets.information);
      this.filtered = [...this.informationT];
    })
  }
  loadTickets(){
    if(!this.startSearchDate || !this.endSearchDate){
      console.log('Date is Missing');
      this.dateISMISSING = true;
      return;
    }
    const startDate = new Date(this.startSearchDate);
    const endDate = new Date(this.endSearchDate);
    this.filtered = this.informationT.filter(item =>{
      return isWithinInterval(new Date(item.date),{start: startDate, end: endDate})
    })
    this.searchmyticket = true;
  }

  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
}
