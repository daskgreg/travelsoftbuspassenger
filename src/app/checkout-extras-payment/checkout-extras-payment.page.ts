import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { WheelSelector } from '@ionic-native/wheel-selector/ngx';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-checkout-extras-payment',
  templateUrl: './checkout-extras-payment.page.html',
  styleUrls: ['./checkout-extras-payment.page.scss'],
})
export class CheckoutExtrasPaymentPage implements OnInit {

  dummyJson = {
    days:[
      {description: 'Luggages'},
      {description: 'Luggages'}
      
    ],
    people:[
      {description: '1'},
      {description: '2'}
    ]
  }

  constructor(private otherhttp: HttpClient,private loadingCtrl:LoadingController, private http:HTTP, private router:Router,public navCtrl: NavController,private selector:WheelSelector,private toast:ToastController) { 
   
  }
  
  voucher:any;
  adults:any;
  children:any;
  enfants:any;
  countSeats = 0;
  toggleStripe=false;
  toggleEmail=true;
  toggleLuggage=false;
  toggleSMS=false;
  toggleSeat=false;
  togglePaypal=false;
  luggageNumber=false;
  ngIFSMS = false;
  select_option:string;
  ngSSON= true;
  ngSSOFF= false;
  tempBookedSeats:any;
  resultoftrp:any;
  trips:any;
  imgTrue = false;
  imgFalse = true;
  totalSeatsPrice: number = 0;
  totalPeople:any;
  buttonshow = false;
  
  paypalPayment = 'false';
  stripePayment = 'false';
  emailVerification = 'true';
  smsVerification = 'false';

  ngOnInit() {
   
  }
  nextStep(){


    if(this.toggleEmail == true){
      this.emailVerification = "TRUE";
    }else{
      this.emailVerification = "TRUE";
    }

    if(this.toggleSMS == true){
      this.smsVerification = "TRUE";
    }else{
      this.smsVerification = "FALSE";
    }
    if(this.togglePaypal == true){
      this.paypalPayment = "TRUE";
    }else{
      this.paypalPayment = "FALSE";
    }
    if(this.toggleStripe == true){
      this.stripePayment = "TRUE";
    }else{
      this.stripePayment = "FALSE";
    }
    localStorage.setItem('emailVerification',this.emailVerification);
    localStorage.setItem('smsVerification',this.smsVerification);
    localStorage.setItem('paypalPayment',this.paypalPayment);
    localStorage.setItem('stripePayment',this.stripePayment);
    

    this.router.navigate(['checkout-summary']);

  }
  ionViewDidEnter(){




    if(this.toggleSMS == true){
      this.toggleSMS = false;

    }else{
      this.toggleSMS = false;

    }



    if(this.toggleEmail == true){
      this.toggleEmail = true;

    }else{
      this.toggleEmail = true;

    }

    if(this.togglePaypal == true){
      this.togglePaypal = false;
    }else{
      this.togglePaypal = false;
    }


    if(this.toggleStripe == true){
      this.toggleStripe = false;
    }else{
      this.toggleStripe = false;
    }

    this.adults = localStorage.getItem('adults');
    this.children = localStorage.getItem('children');
    this.enfants = localStorage.getItem('enfants');

    if( this.adults == undefined){
      this.adults = 0;
    }
    if( this.children == 'undefined'){
      this.children = 0;
    }
    if( this.enfants == 'undefined'){
      this.enfants = 0;
    }

    this.totalPeople = parseFloat(this.adults) + parseFloat(this.children) + parseFloat(this.enfants);

    localStorage.setItem('totalPeople',this.totalPeople);
    
    console.log("total people", this.totalPeople)
  }

  ionToggleHandlerLuggage(){
   
    if(this.toggleLuggage == true){ this.luggageHandler(); }else {this.luggageNumber = false;}
    
  }
  //
  ionToggleHandlerSeat(){

    if(this.toggleSeat == true){ this.seatHandler(); }

  }

  ionToggleHandlerEmail(){

    if(this.toggleEmail == true)  {
      
      this.emailHandler();
    
    } else {
      
      this.emailVerification = 'false'; 
      
      localStorage.setItem('emailVerification',this.emailVerification);
    
    }

  }

  ionToggleHandlerSMS(){

    if(this.toggleSMS == true) { 
      
      this.smsHandler(); 
    
    } else{
      
      this.ngIFSMS = false; 
      
      this.smsVerification = 'false';
      
      localStorage.setItem('smsVerification',this.smsVerification);

      console.log(this.smsVerification);
    
    }
    
  }

  ionToggleHandlerPaypal(){

    if(this.togglePaypal== true) {

      this.paypalHandler();
      
    } else {
       
       this.paypalPayment = 'false'; 

       localStorage.setItem('paypalPayment',this.paypalPayment);

      }

  }

  ionToggleHandlerStripe(){

    if(this.toggleStripe== true) {

      this.stripeHandler();

    }

    }
    
  

  stripeHandler(){

    this.togglePaypal = false;

    this.stripePayment = 'true';

    localStorage.setItem('stripePayment',this.stripePayment);

  }

  paypalHandler(){

    this.toggleStripe = false;

    this.paypalPayment = 'true';


    localStorage.setItem('paypalPayment',this.paypalPayment);

  }

  smsHandler(){

    this.ngIFSMS = true;

    this.smsVerification = 'true';

    localStorage.setItem('smsVerification',this.smsVerification);

    setTimeout(() => {
      this.ngIFSMS = false;//
    }, 5000)

  }

  luggageHandler(){
    
    this.luggageNumber = true; 

    if(this.select_option != undefined){
      console.log(this.select_option);
    }
      localStorage.setItem('luggageNumber',this.select_option);
  }
   seatHandler(){
    
   
      this.ngSSON = false;
      this.ngSSOFF = true;
      this.buttonshow = true;
      this.getSeats(true);
  
  }

  emailHandler(){
    
    this.emailVerification ='true';

    localStorage.setItem('emailVerification',this.emailVerification);
  } 
  
  async getSeats(isFirstCall: boolean) {


      let nativeCall = this.http.get('http://cf11.travelsoft.gr/itourapi/trp_seats_availability_date.cfm?trp_code=AthThes&VersionID=1&VechicleTypeID=22&VehicleVirtualVersionID=1&ticket_date=2021-01-12&userid=dmta', {}, {
        'Content-Type': 'application/json'
      })
      from(nativeCall).pipe(
        finalize(()=> console.log('load trp_seats_availability'))
      )
      .subscribe( result => {
        console.log(result);

        this.tempBookedSeats = new Array<any>();
        this.resultoftrp = JSON.parse(result.data);
        this.trips = this.resultoftrp.TRPS;

        for (var trip of this.trips) {
          // var changeRow;
          trip.flooerColumns = new Array<any>();
          var lengthValue = 0;

          if (trip && trip.DETAILS && trip.DETAILS.length > 0) {
            for (var seat of trip.DETAILS) {
              // local variable
              seat.TEMPBOOK = false;
              if (seat.CHANGEROW == 0 || seat.CHANGEROW == 1) {     
                if (trip.flooerColumns && trip.flooerColumns.length == lengthValue) {
                  var column = new Array<any>();
                  trip.flooerColumns.push(column);
                }

                trip.flooerColumns[lengthValue].push(seat);

                //////////////
                if (seat.CHANGEROW == 1) {
                  lengthValue += 1;
                }

              }

            }
          }
        }
      })

    
      this.otherhttp.get('http://cf11.travelsoft.gr/itourapi/trp_seats_availability_date.cfm?trp_code=AthThes&VersionID=1&VechicleTypeID=22&VehicleVirtualVersionID=1&ticket_date=2021-01-12&userid=dmta')
   
     

      .subscribe(
      result => {

        this.tempBookedSeats = new Array<any>();
        this.resultoftrp = result;
        this.trips = this.resultoftrp.TRPS;
        for (var trip of this.trips) {
          // var changeRow;
          trip.flooerColumns = new Array<any>();
          var lengthValue = 0;

          if (trip && trip.DETAILS && trip.DETAILS.length > 0) {
            for (var seat of trip.DETAILS) {
              // local variable
              seat.TEMPBOOK = false;
              if (seat.CHANGEROW == 0 || seat.CHANGEROW == 1) {
                if (trip.flooerColumns && trip.flooerColumns.length == lengthValue) {
                  var column = new Array<any>();
                  trip.flooerColumns.push(column);
                }
                trip.flooerColumns[lengthValue].push(seat);

                //////////////
                if (seat.CHANGEROW == 1) {
                  lengthValue += 1;
                }

              }

            }
          }
        }

      },
      err => {
        console.error("Error : " + err);

      },
      () => {
        console.log('getData completed');

        setTimeout(function () {
          var pDiv = document.getElementById('ion-scroll');
          var cDiv = pDiv.children;

          for (var i = 0; i < cDiv.length; i++) {
            if (cDiv[i].tagName == "DIV") {   //or use toUpperCase()
              if (cDiv[i]) cDiv[i].setAttribute('style', 'padding:0px'); //do styling here
            }
          }

        }, 500);
      }
    );  
  
   }

   selectSeat(seat: any, floor: number) {
    console.log(seat);
    if (seat.ISBOOKED) {
      seat.TEMPBOOK = false;
      alert('seat is already booked');
      return;
    }
    console.log("Seat", seat);
    console.log("Seat IsBooked", seat.ISBOOKED);

    if (seat.TEMPBOOK) {
      seat.TEMPBOOK = false;
      console.log("temp book seats2:", this.tempBookedSeats.length)
      if (this.totalSeatsPrice && seat.PRICE) this.totalSeatsPrice -= seat.PRICE;
      if (this.tempBookedSeats && this.tempBookedSeats.length > 0) {
        for (var obj of this.tempBookedSeats) {
          if (obj.SEATID == seat.SEATID) {
            var index = this.tempBookedSeats.indexOf(obj);
            this.tempBookedSeats.splice(index, 1);
          }
        }
      }
    } else {
      console.log("temp book seats1:", this.tempBookedSeats.length)
      this.imgTrue = true;
      this.imgFalse = true;
      this.countSeats++;
     

      seat.TEMPBOOK = true;
      if (this.countSeats > this.totalPeople) {
        alert('Not allowed to select more seats');
        this.countSeats--;
        console.log(this.countSeats);
        seat.TEMPBOOK = false;
        this.tempBookedSeats.length--;
        console.log(this.tempBookedSeats.length)
      }else{
        alert('You have book Seat:' + seat.SEATID);
        console.log(this.countSeats);
      }
      console.log("tempBook", seat.TEMPBOOK);
      if (seat.PRICE)
        this.totalSeatsPrice = this.totalSeatsPrice + seat.PRICE;
      var obj: any = {};
      obj.totalSeatsPrice = this.totalSeatsPrice;
      obj.SEATID = seat.SEATID;
      obj.SEATNUMBER = seat.SEATNUMBER;
      obj.FLOOR = floor;
      obj.PRICE = seat.PRICE;
      this.tempBookedSeats.push(obj);
    }
  }
  turnofseats(){
    this.ngSSOFF = false;
    this.ngSSON = true;
    this.toggleSeat = false;
    console.log("temp book seats1:", this.tempBookedSeats.length)

  }
  
  goback(){

    this.router.navigate(['checkout']);

  }
}
