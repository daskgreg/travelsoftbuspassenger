import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutExtrasPaymentPage } from './checkout-extras-payment.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutExtrasPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutExtrasPaymentPageRoutingModule {}
