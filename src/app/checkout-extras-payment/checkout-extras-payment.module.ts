import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutExtrasPaymentPageRoutingModule } from './checkout-extras-payment-routing.module';

import { CheckoutExtrasPaymentPage } from './checkout-extras-payment.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutExtrasPaymentPageRoutingModule,
    TranslateModule
  ],
  declarations: [CheckoutExtrasPaymentPage]
})
export class CheckoutExtrasPaymentPageModule {}
