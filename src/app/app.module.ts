import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { create } from '@angular/language-service';

import { WheelSelector } from '@ionic-native/wheel-selector/ngx';
import { HttpClientModule,HttpClient } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { LanguagePopoverPage } from './language-popover/language-popover.page';
import { LanguagePopoverPageModule } from './language-popover/language-popover.module';
import { HTTP } from '@ionic-native/http/ngx'
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { PayPal } from '@ionic-native/paypal/ngx';
export function createTranslateLoader(http:HttpClient){
  return new TranslateHttpLoader(http,'assets/i18n/', '.json');
}

import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
     IonicModule.forRoot(), 
     AppRoutingModule,
     HttpClientModule,
     IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps:[HttpClient]
      }
    }),
    LanguagePopoverPageModule
  ],
  providers: [  BarcodeScanner,
    Base64ToGallery,
   { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    WheelSelector, HTTP, Keyboard,PayPal,],
  bootstrap: [AppComponent],
})
export class AppModule {}
