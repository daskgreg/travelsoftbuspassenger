import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutSummaryPage } from './checkout-summary.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutSummaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutSummaryPageRoutingModule {}
