import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutSummaryPageRoutingModule } from './checkout-summary-routing.module';

import { CheckoutSummaryPage } from './checkout-summary.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutSummaryPageRoutingModule,
    TranslateModule
  ],
  declarations: [CheckoutSummaryPage]
})
export class CheckoutSummaryPageModule {}
