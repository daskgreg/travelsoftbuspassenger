import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
@Component({
  selector: 'app-checkout-summary',
  templateUrl: './checkout-summary.page.html',
  styleUrls: ['./checkout-summary.page.scss'],
})
export class CheckoutSummaryPage implements OnInit {

dataFrom:any;
dataTo:any;
dataDate:any;
dataArr:any;
dataDepar:any;
totalPrice:any;
dateToshowGen:any;
totalTimeOfTrip:any;
totalPeople:any;

emailCheck:any;
smsCheck:any;
paypalCheck:any;
stripeCheck:any;

NGIFSMS = false;
NGIFEMAIL = false;
NGIFSUITCASE = false;
totalPriceOfTickets:any;
smsPrice = '1.55';
emailPrice = '0.00';
paymentNG = true;
paypalPaymentNG = false;
FFpayment:any;
  constructor(private router:Router,private payPal: PayPal) {
    console.log(this.totalPriceOfTickets);

  }
  paymentAmount: string = '3.33';
  currency: string = 'INR';
  currencyIcon: string = '₹';

 
  ngOnInit() {
  }

  ionViewDidEnter(){



  this.paypalPaymentNG = false;
  this.paymentNG = true;

   this.dataFrom = localStorage.getItem('FPUP_DESC');

   this.dataTo = localStorage.getItem('TPUP_DESC');

   this.dataDate = localStorage.getItem('myDate');

   this.dataArr = localStorage.getItem('DROPPING_TIME');

   this.dataDepar = localStorage.getItem('BOARDING_TIME');

   this.totalPrice = localStorage.getItem('TOTAL_PRICE_WITH_FEE');

   this.dateToshowGen = localStorage.getItem('dateToShowForRoute');
  
   this.totalTimeOfTrip = localStorage.getItem('calucationOfTime');
  
   this.totalPeople = localStorage.getItem('totalPeople');

   this.emailCheck = localStorage.getItem('emailVerification');

   console.log("Email",this.emailCheck);

   this.smsCheck = localStorage.getItem('smsVerification');

   console.log("Sms",this.smsCheck);

   this.paypalCheck =  localStorage.getItem('paypalPayment');

   console.log("Paypal",this.paypalCheck);

   this.stripeCheck =   localStorage.getItem('stripePayment');

   console.log("Stripe",this.stripeCheck);


   if(this.smsCheck == 'TRUE'){

      this.NGIFSMS = true;

      this.totalPriceOfTickets = parseFloat(this.totalPrice) + parseFloat(this.smsPrice);
      this.totalPriceOfTickets.toString();
   }else{

    this.totalPriceOfTickets = parseFloat(this.totalPrice) ;
    this.totalPriceOfTickets.toString();
    console.log(this.totalPriceOfTickets)
   }
   if(this.emailCheck == 'TRUE'){

      this.NGIFEMAIL = true;

    }else{
    
    }

    if(this.paypalCheck == 'TRUE'){
      this.paypalPaymentNG = true; 
    let _this = this;
    setTimeout(() => {
      // Render the PayPal button into #paypal-button-container
      <any>window['paypal'].Buttons({

        // Set up the transaction
        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [{
              amount: {
                value: _this.totalPriceOfTickets
              }
            }]
          });
        },
        //
        // Finalize the transaction
        onApprove: function (data, actions) {
          return actions.order.capture()
            .then(function (details) {
              console.log(data);
              console.log("details",details.status);
              _this.FFpayment = details.status;
              console.log(_this.FFpayment)
              if(details.status == 'COMPLETED'){
               _this.router.navigate(['tickets']);
              
              }
              if(_this.FFpayment == "COMPLETED"){
                alert('2');
               // _this.router.navigate(['main']);
              }
              
            })
            .catch(err => {
              console.log(err);
            })
        }
      }).render('#paypal-button-container');
    }, 500)
  }

  }

  smsFunction(){

  }
  emailFunction(){

  }
  ticketsFunction(){
    
  }
  resultFunction(){

  }
  goback(){
    this.paypalPaymentNG = false;
    this.paymentNG = true;
    this.router.navigate(['/checkout-extras-payment'])
  }
  selectOtherPaymentBtn(){
    this.paypalPaymentNG = false;
    this.paymentNG = true;
    this.router.navigate(['checkout-extras-payment'])
  }
  goToTicketsPage(){

    if(this.paypalCheck == 'TRUE'){
    this.paypalPaymentNG = true;
    this.paymentNG = false;
    }else if (this.stripeCheck == 'TRUE'){
      alert('Stripe');
    }else if ((this.paypalCheck == 'FALSE') && (this.stripeCheck == 'FALSE')){
      alert('Ξεχάσες να επιλέξεις πληρωμή');
    }

  }//
}
