import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { LanguagePopoverPage } from '../language-popover/language-popover.page';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(private router: Router, private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }
  async openLanguagePopover(ev){
    const popover = await this.popoverCtrl.create({
      component:LanguagePopoverPage,
      event:ev
    });
    await popover.present();
  }
  navigateToProfile(){
    this.router.navigate(['profile']);
  }
  navigateToServices(){
    this.router.navigate(['services']);
  }
  navigateToLanguage(){
    this.router.navigate(['language']);
  }
  navigateToPrivacyPolicy(){
    
  }
  navigateToTermsAndConditions(){

  }
  navigateToFAQ(){
    
  }
  navigateToLogin(){
    this.router.navigate(['login']);
  }
  navigateToHome(){
    this.router.navigate(['home']);
  }
  navigateToTickets(){
    this.router.navigate(['tickets']);
  }
  navigateToSettings(){
    this.router.navigate(['settings']);
  }
  navigateToTest(){
    this.router.navigate(['test']);
  }
}
