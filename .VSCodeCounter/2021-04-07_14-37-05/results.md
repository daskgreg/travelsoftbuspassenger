# Summary

Date : 2021-04-07 14:37:05

Directory /Users/gregd/Desktop/Gregs Workspace/Work/TravelSoft/BusPassenger2021/BusPassenger1stApril/src/app

Total : 122 files,  4736 codes, 69 comments, 915 blanks, all 5720 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 82 | 2,343 | 27 | 641 | 3,011 |
| SCSS | 20 | 1,314 | 0 | 78 | 1,392 |
| HTML | 20 | 1,079 | 42 | 196 | 1,317 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 122 | 4,736 | 69 | 915 | 5,720 |
| checkout | 6 | 259 | 12 | 78 | 349 |
| checkout-extras-payment | 6 | 559 | 19 | 128 | 706 |
| checkout-summary | 6 | 314 | 0 | 41 | 355 |
| forgot-password | 6 | 67 | 0 | 24 | 91 |
| home | 6 | 466 | 1 | 64 | 531 |
| language | 6 | 209 | 0 | 28 | 237 |
| language-popover | 6 | 79 | 0 | 21 | 100 |
| livechat | 6 | 67 | 0 | 24 | 91 |
| login | 6 | 191 | 0 | 59 | 250 |
| main | 6 | 145 | 0 | 29 | 174 |
| profile | 6 | 214 | 0 | 41 | 255 |
| register | 6 | 167 | 0 | 37 | 204 |
| routes | 6 | 263 | 0 | 64 | 327 |
| service | 2 | 45 | 0 | 13 | 58 |
| services | 6 | 168 | 0 | 26 | 194 |
| settings | 6 | 248 | 0 | 27 | 275 |
| test | 6 | 259 | 36 | 76 | 371 |
| tickets | 6 | 260 | 0 | 30 | 290 |
| tickets-mytickets | 6 | 267 | 0 | 36 | 303 |
| tickets-search | 6 | 317 | 0 | 47 | 364 |

[details](details.md)